/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.currentDevice;
import static KeywordDrivenTestFramework.Core.BaseClass.currentPlatform;
import static KeywordDrivenTestFramework.Core.BaseClass.relativeScreenShotPath;
import static KeywordDrivenTestFramework.Core.BaseClass.reportDirectory;
import static KeywordDrivenTestFramework.Core.BaseClass.setScreenshotPath;
import static KeywordDrivenTestFramework.Core.BaseClass.testCaseId;
import KeywordDrivenTestFramework.Entities.Enums;
import java.net.URL;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import KeywordDrivenTestFramework.Entities.RetrievedTestValues;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Reporting.Narrator;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Key;
import sun.misc.BASE64Decoder;

/**
 *
 * @author sbeck
 */
public class AppiumDriverUtility extends BaseClass
{

    //Appium Driver is the Superclass of mobile drivers allowing both iOS and Android
    //This comes with a downside that it blocks SOME bindings from both systems.
    public AppiumDriverLocalService service;
    public static AppiumDriver Driver;


    File fileIEDriver;
    public RetrievedTestValues retrievedTestValues;
    public String DriverExceptionDetail = "";
    public String Port = "";
    TestEntity testData;
    public Boolean _isDriverRunning = false;
    Runtime rt = Runtime.getRuntime();
    public static boolean isCMDRunning = false;

    public AppiumDriverUtility()
    {
        //This should be set in the testsuite, _isRemote should be true if the appium server is hosted outside of this automation.
        if (_isRemote == false)
        {
            //This is used for multiple test runs, rather than relaunching appium every iteration. isCMDRunning should become true, once the server is launched.
            if (isCMDRunning == false)
            {
                //selectPlatform will decide which system to run and then initialise the correct Appium server.
                selectPlatform();
            }
            _isDriverRunning = true;
        }
        else
        {
            switch (currentPlatform)
            {
                case Android:
                    Driver = (AndroidDriver) setCapabilitiesRemoteRun(currentDevice.DeviceID, currentDevice.ServerURL);
                    _isDriverRunning = true;
                    break;

                case iOS:
                    Driver = (IOSDriver) setCapabilitiesWithIpa();
                    _isDriverRunning = true;
                    break;
            }

        }

    }

    public WebDriver setCapabilitiesWithApk()
    {

        try
        {
            //The APK information is set in the appconfig
            File app = new File(currentDeviceConfig.ApplicationFilePath, currentDeviceConfig.ApplicationName);

            DesiredCapabilities capabilities = new DesiredCapabilities();

            capabilities.setCapability(currentDevice.CapabilityName, currentDevice.DeviceID);
            capabilities.setCapability("deviceName", currentDeviceConfig.deviceName);
            capabilities.setCapability("platformName", currentDeviceConfig.platformName);
            capabilities.setCapability("automationName", currentDeviceConfig.automationName);
            capabilities.setCapability(CapabilityType.VERSION, currentDeviceConfig.Version);
//            Call the following use RunActivity
            capabilities.setCapability("appPackage", currentDeviceConfig.appPackage);
            capabilities.setCapability("appActivity", currentDeviceConfig.appActivity);
            capabilities.setCapability("app", app.getAbsolutePath());
            capabilities.setCapability("newCommandTimeout", 360);
            isCMDRunning = true;
            return new AndroidDriver(new URL("http://" + currentDevice.ServerURL + ":" + appiumPort + "/wd/hub"), capabilities);
        }

        catch (Exception e)
        {
            Narrator.logError("Error setting Capabilities - " + e.getMessage());
            return null;
        }
    }

    public WebDriver setCapabilitiesWithIpa()
    {
        //PLEASE MAKE SURE NO OTHER CAPABILITIES ARE ADDED, ALL CAPABILITIES ARE NOT SUPPORTED BY IOS -- Mugammad
        try
        {
            //The IPA information is set in the appconfig
            File app = new File(currentDeviceConfig.ApplicationFilePath, currentDeviceConfig.ApplicationName);

            DesiredCapabilities capabilities = new DesiredCapabilities();
            String url = currentDevice.ServerURL + ":" + currentDevice.ServerPort;
            capabilities.setCapability(currentDevice.CapabilityName, currentDevice.DeviceID);
            capabilities.setCapability("deviceName", currentDeviceConfig.deviceName);
            capabilities.setCapability("platformName", currentDeviceConfig.platformName);
            capabilities.setCapability("automationName", currentDeviceConfig.automationName);
            capabilities.setCapability("app", app.getAbsolutePath());
            capabilities.setCapability("newCommandTimeout", 2000);
            capabilities.setCapability("fullReset", "true");

            return new IOSDriver<>(new URL(url), capabilities);
        }

        catch (Exception e)
        {
            Narrator.logError("Error setting Capabilities - " + e.getMessage());
            return null;
        }
    }

    public WebDriver setCapabilitiesRemoteRun(String deviceUDID, String serverURL)
    {

        try
        {
            DesiredCapabilities capabilities = new DesiredCapabilities();

            capabilities.setCapability("udid", deviceUDID);
            capabilities.setCapability("deviceName", deviceUDID);
            capabilities.setCapability("appPackage", currentDeviceConfig.appPackage);
            capabilities.setCapability("appActivity", currentDeviceConfig.appActivity);
            capabilities.setCapability("platformName", currentDevice.platform);
            capabilities.setCapability("newCommandTimeout", 360);
            capabilities.setCapability("onResetTimeout", 360);

            return new RemoteWebDriver(new URL(serverURL), capabilities)
            {
            };
        }

        catch (Exception e)
        {
            Narrator.logError("Error setting Capabilities - " + e.getMessage());
            return null;
        }
    }

    public void startAndroidServer(String directory)
    {
        try
        {
            //Old Appium Server implementation
//            Port = currentDevice.ServerURL;
//            String new_dir = directory;
//            String[] URLinfo = Port.split(":");
//            String[] Portinfo = URLinfo[2].split("/");
//            String PortNumber = Portinfo[0];

//            rt.exec("cmd.exe /c cd \"" + new_dir + "\" & start cmd.exe /k \""+appiumTest+" -p \"" + PortNumber + "\" -bp " + appiumBootstrapPort + " -U " + currentDevice.DeviceID + "\"");
            AppiumServiceBuilder builder;
            String serverURL;

            builder = new AppiumServiceBuilder();
            builder.withIPAddress(currentDevice.ServerURL);

            if (currentDevice.ServerPort.isEmpty())
            {
                builder.usingAnyFreePort();
            }
            else
            {
                builder.usingPort(Integer.parseInt(currentDevice.ServerPort));
            }

            //Adds bootstrap port if needed
//            builder.withArgument(AndroidServerFlag.BOOTSTRAP_PORT_NUMBER, appiumBootstrapPort);
            service = AppiumDriverLocalService.buildService(builder);
            service.start();

            serverURL = service.getUrl().toString();

            if (currentDevice.ServerPort.isEmpty())
            {
                String[] URLinfo = serverURL.split(":");
                String[] Portinfo = URLinfo[2].split("/");
                appiumPort = Portinfo[0];
            }
            else
            {
                appiumPort = currentDevice.ServerPort;
            }

            Narrator.logDebug("Started Android Appium Server on " + serverURL);
        }

        catch (Exception e)
        {
            Narrator.logError("Failed to start server - " + e.getMessage());
        }

    }

    //Mugammad Added
    public boolean startiOSServer()
    {
        try
        {
            String tellCommand = "tell application \"Terminal\" to do script \"appium\"";

            String[] command =
            {
                "osascript", "-e",
                tellCommand
            };

            ProcessBuilder pBuilder = new ProcessBuilder(command);
            pBuilder.start();

            pause(8000);

        }
        catch (Exception e)
        {
            Narrator.logError("Failed to start server - " + e.getMessage());
        }

        return true;
    }

    //Mugammad Added
    public boolean quitTerminal()
    {
        try
        {
            String tellCommand = "tell application \"Terminal\" to quit";

            String[] command =
            {
                "osascript", "-e",
                tellCommand
            };

            ProcessBuilder pBuilder = new ProcessBuilder(command);
            pBuilder.start();

            System.out.println("Terminal exited successfully.");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to quit terminal - " + e.getMessage());
            return false;
        }
    }

    public boolean isDriverRunning()
    {
        return _isDriverRunning;
    }

    public void selectPlatform()
    {
        switch (currentDevice.platform)
        {
            case Android:
                startAndroidServer(currentDevice.ServerURL);
                int counter = 1;
                Driver = (AndroidDriver) setCapabilitiesWithApk();
                while (Driver == null && counter < 61)
                {
                    Driver = (AndroidDriver) setCapabilitiesWithApk();
                    pause(500);
                    counter++;

                }
                _isDriverRunning = true;
                break;
            case iOS:
                startiOSServer();
                Driver = (IOSDriver) setCapabilitiesWithIpa();
                _isDriverRunning = true;
                break;
        }
        retrievedTestValues = new RetrievedTestValues();
    }

    public void Reset()
    {
        try
        {

            Runtime close = Runtime.getRuntime();
            close.exec("taskkill /IM cmd.exe");
        }
        catch (Exception e)
        {
            Narrator.logError("Error restarting the driver - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }

    }

    public void stopAndroidServer()
    {
        try
        {
            service.stop();
            Narrator.logDebug("Stopped Android Appium Server on " + service.getUrl().toString());
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to stop server - " + e.getMessage());
        }
    }

    //Mugammad Added
    public void stopIOSServer()
    {
        String[] killNodes =
        {
            "/usr/bin/killall", "-9", "node"
        };
        String[] killProxy =
        {
            "/usr/bin/killall", "-9", "ios_webkit_debug_proxy"
        };
        try
        {
            Runtime.getRuntime().exec(killNodes);
            System.out.println("Appium server stopped.");
            Runtime.getRuntime().exec(killProxy);
            System.out.println("iProxy server stopped.");
            quitTerminal();
        }
        catch (Exception e)
        {
            Narrator.logError("Error stopping iOS server - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }

        quitTerminal();
    }

    public String get(final String url)
    {
        Driver.get(url);
        return Driver.getPageSource();

    }

//    public boolean elementGestureTest(String Id)
//    {
//        //SikuliDriverInstance.Desktop.find(SikuliDriverInstance.ScreenshotDirectory + ImageFilePath).sw;
//        MobileElement e = (MobileElement) Driver.findElementById(Id);
//
//        //SikuliDriverInstance.Desktop.find(SikuliDriverInstance.ScreenshotDirectory + ImageFilePath);
//        e.swipe(io.appium.java_client.SwipeElementDirection.UP, 2000);
//        e.swipe(io.appium.java_client.SwipeElementDirection.DOWN, 2000);
//        return true;
//    }
    public enum SwipeElementDirection
    {
        /**
         * Up from the center of the lower
         */
        UP
        {
            @Override
            void swipe(AppiumDriver driver, MobileElement element, int duration)
            {
                Point p = element.getCenter();
                Point location = element.getLocation();
                Dimension size = element.getSize();
                driver.swipe(p.getX(), location.getY() + size.getHeight(), p.getX(), location.getY(), duration);
            }
        },
        /**
         * Down from the center of the upper
         */
        DOWN
        {
            @Override
            void swipe(AppiumDriver driver, MobileElement element, int duration)
            {
                Point p = element.getCenter();
                Point location = element.getLocation();
                Dimension size = element.getSize();
                driver.swipe(p.getX(), location.getY(), p.getX(), location.getY() + size.getHeight(), duration);
            }
        },
        /**
         * To the left from the center of the rightmost
         */
        LEFT
        {
            @Override
            void swipe(AppiumDriver driver, MobileElement element, int duration)
            {
                Point p = element.getCenter();
                Point location = element.getLocation();
                Dimension size = element.getSize();
                driver.swipe(location.getX() + size.getWidth(), p.getY(), location.getX(), p.getY(), duration);
            }
        },
        /**
         * To the right from the center of the leftmost
         */
        RIGHT
        {
            @Override
            void swipe(AppiumDriver driver, MobileElement element, int duration)
            {
                Point p = element.getCenter();
                Point location = element.getLocation();
                Dimension size = element.getSize();
                driver.swipe(location.getX(), p.getY(), location.getX() + size.getWidth(), p.getY(), duration);
            }
        };

        void swipe(AppiumDriver driver, MobileElement element, int duration)
        {
        }
    }

    public boolean scrollDown(String element)

    {
        try
        {
            JavascriptExecutor js = (JavascriptExecutor) Driver;
            System.out.println("swiping to element " + element);
            js.executeScript("mobile: scrollTo", element);
            Driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            return true;

        }
        catch (Exception e)
        {
            Narrator.logError("Could not find verification text - " + e.getMessage());
            return false;
        }
    }

    public boolean verifyIfScreenIsLoaded(String xpath, String verificationText)
    {
        try
        {
            String actionBarTitle = Driver.findElement(By.xpath(xpath)).getText();
            if (actionBarTitle.toUpperCase().trim().equals(verificationText.toUpperCase().trim()))
            {
                Narrator.logDebug("Verification text found...Screen loaded successfully");
                return true;
            }
            else
            {
                Narrator.logError("Could not find verification text - ");
                return false;
            }

        }
        catch (Exception e)
        {
            Narrator.logError("Could not find verification text - " + e.getMessage());
            return false;
        }
    }

    public boolean switchToFrameByXpath(String xpath)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Driver.switchTo().frame(xpath);
                    return true;
                }
                catch (Exception e)
                {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError("Error switching to frame by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToLastDuplicateFrameByXpath(String Xpath)
    {
        int waitCount = 0;
        try
        {
            this.switchToDefaultContent();
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    List<WebElement> iframes = Driver.findElements(By.xpath(Xpath));

                    Driver.switchTo().frame((WebElement) iframes.toArray()[iframes.size() - 1]);
                    return true;
                }
                catch (Exception e)
                {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError("Error switching to last duplicate frame by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToDefaultContent()
    {
        try
        {
            Driver.switchTo().defaultContent();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error switching to default content  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean moveToElementByXpath(String Xpath)
    {
        try
        {
            Actions moveTo = new Actions(Driver);
            moveTo.moveToElement(Driver.findElement(By.xpath(Xpath)));
            moveTo.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error moving to element - " + Xpath + " - " + e.getStackTrace());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToDefaultContentWhenElementNoLongerVisible(String previousFrameXpath)
    {
        try
        {
            waitForElementNoLongerPresentByXpath(previousFrameXpath);
            Driver.switchTo().defaultContent();
            System.out.println("Successfully switched to default content, current frame handle = " + Driver.getWindowHandle() + ", previous frameId - " + previousFrameXpath);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error switching to default content when element is no longer visible - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementNoLongerPresentByXpath(String elementId)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) == null)
                    {
                        elementFound = false;
                        break;
                    }
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }

                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean acceptAlertDialog()
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Driver.switchTo().alert().accept();
                    return true;
                }
                catch (Exception e)
                {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError("Error accepting alert dialog - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByValueFromDropDownListUsingXpath(String Xpath, String valueToSelect)
    {
        try
        {
            this.waitForElementByXpath(Xpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(Xpath)));
            dropDownList.selectByValue(valueToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error selecting from dropdownlist by value using Name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByTextFromDropDownListUsingXpath(String Xpath, String Text)
    {
        try
        {
            this.waitForElementByXpath(Xpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(Xpath)));
            dropDownList.selectByVisibleText(Text);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error selecting from dropdownlist by text using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void waitUntilElementEnabledByXpath(String ElementXpath)
    {
        try
        {
            int counter = 0;
            boolean isEnabled = false;
            WebElement elementToWaitFor = Driver.findElement(By.xpath(ElementXpath));
            isEnabled = elementToWaitFor.isEnabled();
            while (!isEnabled && counter < 60)
            {
                counter++;
                Thread.sleep(500);
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Error waiting for element to be enabled - " + e.getMessage());
        }

    }

    public boolean checkBoxSelectionByXpath(String elementXpath, boolean mustBeSelected)
    {
        try
        {
            Thread.sleep(2000);
            this.waitForElementByXpath(elementXpath);
            this.waitUntilElementEnabledByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, 60);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement checkBox = Driver.findElement(By.xpath(elementXpath));
            if (checkBox.isSelected() != mustBeSelected)
            {
                checkBox.click();
            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error selecting checkbox by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean navigateToPreviousScreen()
    {
        try
        {
            Driver.navigate().back();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to navigate to the previous screen - " + e.getMessage());
            return false;
        }

    }

    public boolean validateElementTextValueByXpath(String elementXpath, String elementText)
    {
        try
        {
            if (waitForElementByXpath(elementXpath))
            {
                WebElement elementToValidate = Driver.findElement(By.xpath(elementXpath));
                String textDetected = elementToValidate.getText();
                if (textDetected.contains(elementText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Error validating element text value by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    //IOS
    public boolean validateIOSTextByXpath(String elementXpath, String elementText)
    {
        try
        {
            if (waitForElementByXpath(elementXpath))
            {
                WebElement elementToValidate = Driver.findElement(By.xpath(elementXpath));
                String textDetected = elementToValidate.getAttribute("value");
                if (textDetected.contains(elementText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Error validating element text value by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void WaitUntilDropDownListPopulatedByXpath(String elementXpath)
    {

        try
        {
            this.waitForElementByXpath(elementXpath);
            int waitCount = 0;
            List<WebElement> optionsList;
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
                    optionsList = dropDownList.getOptions();
                    if (optionsList.size() > 0)
                    {
                        break;
                    }
                }
                catch (Exception e)
                {

                }
                Thread.sleep(500);
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Error waiting for dropdownlist to be populated by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public boolean clickElementbyXpath(String elementXpath)
    {
        try
        {
            waitForElementByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, 60);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean checkElementPresenceByXpath(String elementXpath)
    {
        boolean returnValue = false;
        try
        {
            waitForElementByXpath(elementXpath);
            WebElement element = Driver.findElement(By.xpath(elementXpath));

            if (element.isDisplayed())
            {
                returnValue = true;
            }
        }
        catch (Exception e)
        {
            returnValue = false;
        }
        return returnValue;
    }

    public boolean clickElementbyId(String id)
    {
        try
        {
            waitForElementById(id);
            WebDriverWait wait = new WebDriverWait(Driver, 60);
            wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
            WebElement elementToClick = Driver.findElement(By.id(id));
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByXpath(String elementXPath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXPath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXPath));
            elementToTypeIn.sendKeys(textToEnter);
            return true;
        }
        catch (Exception e)
        {
            System.err.println("Error entering text - " + elementXPath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clearTextByXpath(String elementXPath)
    {
        try
        {
            this.waitForElementByXpath(elementXPath);
            WebElement elementToClear = Driver.findElement(By.xpath(elementXPath));

            String text = elementToClear.getAttribute("text");
            int textLength = text.length();
            while (textLength >= 0)
            {
                text = elementToClear.getAttribute("text");
                if (text.length() != 0)
                {
                    elementToClear.sendKeys(Keys.DELETE);
                    textLength--;
                }
                else
                {
                    textLength = -1;
                }
            }
            return true;
        }
        catch (Exception e)
        {
            System.err.println("Error clearing text - " + elementXPath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < 30)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpath(String elementXpath, int timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementById(String id)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + id + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public void pause(int milisecondsToWait)
    {
        try
        {
            Thread.sleep(milisecondsToWait);
        }
        catch (Exception e)
        {

        }
    }

    public void takeScreenShot(String screenShotDescription, boolean isError)
    {
        String imageFilePathString = "";

        if (testCaseId == null)
        {
            return;
        }

        if (Driver == null)
        {
            return;
        }

        try
        {
            StringBuilder imageFilePathBuilder = new StringBuilder();
            // add date time folder and test case id folder
            imageFilePathBuilder.append(AppiumDriverUtility.reportDirectory + "\\");
            // + testCaseId + "\\"

            relativeScreenShotPath = testCaseId + "\\";

            if (isError)
            {
                imageFilePathBuilder.append("FAILED_");

            }
            else
            {
                imageFilePathBuilder.append("PASSED_");

            }

            relativeScreenShotPath += testCaseId + "_" + screenShotDescription + ".png";

            imageFilePathBuilder.append(relativeScreenShotPath);

            if (imageFilePathBuilder.toString().contains("FAILED_"))
            {
                relativeScreenShotPath = "FAILED_" + relativeScreenShotPath;
            }
            else if (imageFilePathBuilder.toString().contains("PASSED_"))
            {
                relativeScreenShotPath = "PASSED_" + relativeScreenShotPath;
            }

            //imageFilePathBuilder.append(this.generateDateTimeString() + ".png");
            imageFilePathString = imageFilePathBuilder.toString();

            setScreenshotPath(imageFilePathString);

            if (AppiumDriverInstance != null && AppiumDriverInstance.isDriverRunning())
            {

                File screenShot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(screenShot, new File(imageFilePathString.toString()));
            }
        }
        catch (WebDriverException | IOException e)
        {
            Narrator.logError("[Error] could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }

    public void takeScreenShotForAppium(String screenShotDescription)
    {
        String imageFilePathString = "";

        if (testCaseId == null)
        {
            return;
        }

        if (Driver == null)
        {
            return;
        }

        try
        {
            StringBuilder imageFilePathBuilder = new StringBuilder();
            // add date time folder and test case id folder
            // + testCaseId + "\\"

            relativeScreenShotPath = screenShotDescription + ".png";

            imageFilePathBuilder.append("AppiumScreenshots\\" + relativeScreenShotPath);

            //imageFilePathBuilder.append(this.generateDateTimeString() + ".png");
            imageFilePathString = imageFilePathBuilder.toString();

            setScreenshotPath(imageFilePathString);

            if (AppiumDriverInstance != null && AppiumDriverInstance.isDriverRunning())
            {

                File screenShot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(screenShot, new File(imageFilePathString.toString()));
            }
        }
        catch (WebDriverException | IOException e)
        {
            Narrator.logError("[Error] could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }

    //IOS
    public void takeScreenShotForAppiumIOS(String screenShotDescription, boolean isError)
    {
        String imageFilePathString = "";

        if (testCaseId == null)
        {
            return;
        }

        if (Driver == null)
        {
            return;
        }

        try
        {
            StringBuilder imageFilePathBuilder = new StringBuilder();

            String reportFolder = System.getProperty("user.dir") + "/" + reportDirectory;
            imageFilePathBuilder.append(reportFolder + "/");
//            
            if (isError)
            {
                relativeScreenShotPath = "FAILED_";
            }
            else
            {
                relativeScreenShotPath = "PASSED_";
            }
            //testCaseId + "_" + 

            relativeScreenShotPath += screenShotDescription;

            String dateTime = this.generateDateTimeString();
            relativeScreenShotPath += dateTime + ".png";
            imageFilePathBuilder.append(relativeScreenShotPath);

//            imageFilePathBuilder.append(dateTime+ ".png");
            imageFilePathString = imageFilePathBuilder.toString();
//            String filePath = reportFolder+"/";
//            String imageName = imageFilePathString.replace(filePath+"/", "");
            setScreenshotPath(imageFilePathString);

            String base64Image = Driver.getScreenshotAs(OutputType.BASE64);
            File file = convertBase64ToImage(base64Image, imageFilePathString, "png");
        }
        catch (Exception e)
        {
            Narrator.logError("[Error] could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }

    public String generateDateTimeString()
    {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");

        return dateFormat.format(dateNow).toString();
    }

    public void CloseChromeInstances() throws IOException
    {
        if (browserType.equals(Enums.BrowserType.Chrome))
        {
            String TASKLIST = "tasklist";
            String KILL = "taskkill /IM ";
            String line;
            String serviceName = "chrome.exe";
            Process p = Runtime.getRuntime().exec(TASKLIST);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            Narrator.logDebug("Closing Chrome tasks...");
            while ((line = reader.readLine()) != null)
            {

                if (line.contains(serviceName))
                {
                    Runtime.getRuntime().exec(KILL + serviceName);
                }
            }
        }
    }

    public boolean TabAndEnterText(String textToEnter)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(Key.TAB, textToEnter);
            action.perform();
            return true;

        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to Tab " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void zoom(int timesToZoom, int Touch1StartX, int Touch1StartY, int Touch1EndX, int Touch1EndY, int Touch2StartX, int Touch2StartY, int Touch2EndX, int Touch2EndY)
    {
        try
        {
            for (int i = 0; i < timesToZoom; i++)
            {

                MultiTouchAction mta = new MultiTouchAction(Driver);
                TouchAction touch1 = new TouchAction(Driver).press(Touch1StartX, Touch1StartY).moveTo(Touch1EndX, Touch1EndY).release();
                TouchAction touch2 = new TouchAction(Driver).press(Touch2StartX, Touch2StartY).moveTo(Touch2EndX, Touch2EndY).release();

                mta.add(touch1).add(touch2).perform();
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            Narrator.logError(" Failed to zoom " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    //IOS
    public String retrieveValueByXpath(String xpath)
    {
        String retrievedText = "";
        try
        {
            waitForElementByXpath(xpath);
            WebElement elementToRead = Driver.findElement(By.xpath(xpath));
            retrievedText = elementToRead.getAttribute("text");
            return retrievedText;
        }
        catch (Exception e)
        {
            System.err.println("Error reading text from element - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveTextByXpath(String xpath)
    {
        String retrievedText = "";
        try
        {
            waitForElementByXpath(xpath);
            WebElement elementToRead = Driver.findElement(By.xpath(xpath));
            retrievedText = elementToRead.getText();
            return retrievedText;
        }
        catch (Exception e)
        {
            System.err.println("Error reading text from element - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public ArrayList<Integer> getElementBounds(String elementXPath)
    {
        try
        {
            ArrayList<Integer> bounds = new ArrayList<>();
            this.waitForElementByXpath(elementXPath);

            WebElement elementToGetBounds = Driver.findElement(By.xpath(elementXPath));
            int startX = elementToGetBounds.getLocation().getX();
            int startY = elementToGetBounds.getLocation().getY();

            bounds.add(startX);
            bounds.add(startY);
            return bounds;
        }
        catch (Exception e)
        {
            narrator.logError("Failed to get element bounds.");
            return null;
        }
    }

    public boolean holdAndASlideElementByXpath(String elementXPath, int startX, int startY, int endX, int endY)
    {
        try
        {
            this.waitForElementByXpath(elementXPath);

            AppiumDriverInstance.Driver.swipe(startX, startY, endX, endY, 3000);

            return true;
        }
        catch (Exception e)
        {
            narrator.logError("Failed to slide option.");
            return false;
        }
    }

    public boolean holdAndASlideElementByXpath(String elementXPath, int xDirection, int yDirection)
    {
        WebElement elementToSlide = Driver.findElement(By.xpath(elementXPath));
        int elementX = elementToSlide.getLocation().x;
        int elementY = elementToSlide.getLocation().y;
        int endX = elementX + xDirection;
        int endY = elementY + yDirection;

        try
        {
            this.waitForElementByXpath(elementXPath);

            AppiumDriverInstance.Driver.swipe(elementX, elementY, endX, endY, 3000);

            return true;
        }
        catch (Exception e)
        {
            narrator.logError("Failed to slide option.");
            return false;
        }
    }

    public boolean clickOnCoOrdinates(int x, int y)
    {
        try
        {
            AppiumDriverInstance.Driver.tap(1, x, y, 500);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error failed to click on ");
            return false;
        }
    }

    public boolean SetOrientationPortrait()
    {
        try
        {
            ScreenOrientation orientation = Driver.getOrientation();
            if (orientation.value().equals(ScreenOrientation.PORTRAIT))
            {
                narrator.logDebug("Tried to change orientation to Portrait, but Orientation already was Portrait.");
                return true;
            }
            else
            {
                Driver.rotate(ScreenOrientation.PORTRAIT);
                narrator.logDebug("Screen orientation set to portrait.");
                return true;
            }
        }
        catch (Exception e)
        {
            narrator.logError("Failed to change screen orientation to Portrait.");
            return false;
        }
    }

    public boolean SetOrientationLandscape()
    {
        try
        {
            ScreenOrientation orientation = Driver.getOrientation();
            if (orientation.value().equals(ScreenOrientation.LANDSCAPE))
            {
                narrator.logDebug("Tried to change orientation to Landscape, but Orientation already was Landscape.");
                return true;
            }
            else
            {
                Driver.rotate(ScreenOrientation.LANDSCAPE);
                narrator.logDebug("Screen orientation set to Landscape.");
                return true;
            }
        }
        catch (Exception e)
        {
            narrator.logError("Failed to change screen orientation to Landscape.");
            return false;
        }
    }

    public boolean verifyThatItemHasLoaded(String imageName, int timeout)
    {
        boolean returnStatement = false;
        boolean isLoaded = false;

        while (!isLoaded && timeout > 0)
        {
            if (SikuliDriverInstance.WaitForElementToAppear(imageName))
            {
                isLoaded = true;
                returnStatement = false;
            }
            else
            {
                isLoaded = false;
                timeout--;
            }
        }
        if (timeout == 0)
        {
            returnStatement = false;
        }

        return returnStatement;
    }

    public boolean verifyThatMapHasLoaded(int timeout)
    {
        boolean returnStatement = false;
        boolean isLoaded = false;

        switch (currentPlatform)
        {
            case Android:
            {
                while (!isLoaded && timeout > 0)
                {
                    if (!SikuliDriverInstance.WaitForElementToAppear("[Android] Loading images.png"))
                    {
                        isLoaded = true;
                        return returnStatement = true;
                    }
                    else
                    {
                        isLoaded = false;
                        timeout--;
                    }
                }
                if (timeout == 0 && isLoaded == false)
                {
                    returnStatement = false;
                }
                break;
            }

            case iOS:
            {
                while (!isLoaded && timeout > 0)
                {
                    if (!SikuliDriverInstance.WaitForElementToAppear("[iOS] Loading images.png"))
                    {
                        isLoaded = true;
                        return returnStatement = true;
                    }
                    else
                    {
                        isLoaded = false;
                        timeout--;
                    }
                }
                if (timeout == 0 && isLoaded == false)
                {
                    returnStatement = false;
                }
                break;
            }
        }

        return returnStatement;
    }

    //IOS
    public void getPageObjects(String fileName)
    {
        try
        {
            File file = new File(System.getProperty("user.dir") + "/iOS PageObjects/" + fileName + ".xml");
            FileWriter fw = new FileWriter(file);
            fw.append(AppiumDriverInstance.Driver.getPageSource());
            fw.close();
        }
        catch (Exception e)
        {
            Narrator.logError("[ERROR] - Failed to write to file - " + e.getMessage());
        }
    }

    //IOS
    public File convertBase64ToImage(String base64Image, String absolutePathToImage, String imageType)
    {
        File outputfile = null;
        try
        {
//            absolutePathToImage = "/Users/gtc/tracker_keyworddriventestautomation_dvt/KeywordDrivenTestReports/i O S  Add  Expenses_2016-11-11_12-41-32/PASSED_example_2016-11-11_12-42-31.png";
            BufferedImage image = null;
            byte[] imageByte;

            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(base64Image);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();

            // write the image to a file
            outputfile = new File(absolutePathToImage);
            FileUtils.forceMkdir(outputfile);
            ImageIO.write(image, imageType, outputfile);
        }
        catch (Exception e)
        {
            Narrator.logError("[ERROR] - Failed to convert Base64 to Image - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }

        return outputfile;
    }

    public int getElementBounds(String elementXPath, String axis)
    {
        int bound = 0;
        try
        {
            waitForElementByXpath(elementXPath);
            WebElement elementToGetBounds = Driver.findElement(By.xpath(elementXPath));
            Point elementLocation = elementToGetBounds.getLocation();

            axis = axis.toLowerCase();
            switch (axis)
            {
                case "x":
                {
                    bound = elementLocation.x;
                    break;
                }

                case "y":
                {
                    bound = elementLocation.y;
                    break;
                }

                default:
                {
                    bound = 0;
                }
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Element not found - " + e.getMessage());
            return bound;
        }
        return bound;
    }

    public boolean enterTextByID(String elementID, String textToEnter)
    {
        try
        {
            this.waitForElementByID(elementID);
            WebElement elementToTypeIn = Driver.findElement(By.id(elementID));
            elementToTypeIn.sendKeys(textToEnter);

            return true;
        }
        catch (Exception e)
        {
            System.err.println("Error entering text - " + elementID + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementByID(String elementID)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementID))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by ID '" + elementID + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;

    }

    public boolean waitForElementByIDWithTimeout(String elementID, int timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementID))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by ID '" + elementID + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public String retrieveTextByID(String elementID)
    {
        String retrievedText = "";
        try
        {

            this.waitForElementByXpath(elementID);
            WebElement elementToRead = Driver.findElement(By.id(elementID));
            retrievedText = elementToRead.getText();
            System.out.println("[Info]Text retrieved successfully from element - " + elementID);
            return retrievedText;

        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to retrieve text from element ID - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public boolean waitForElementNoLongerPresentByID(String elementId)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) == null)
                    {
                        elementFound = false;
                        break;
                    }
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }

                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementsNoLongerPresentByXpath(String elementXpath)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) == null)
                    {
                        elementFound = false;
                        break;
                    }
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }

                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean holdAndSlideByCoOrdinates(int startX, int startY, int endX, int endY)
    {
        try
        {
            AppiumDriverInstance.Driver.swipe(startX, startY, endX, endY, 3000);

            return true;
        }
        catch (Exception e)
        {
            narrator.logError("Failed to scroll");
            return false;
        }
    }

}
