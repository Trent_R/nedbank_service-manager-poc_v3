package KeywordDrivenTestFramework.Entities;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import static java.lang.System.err;

/**
 * Created with IntelliJ IDEA. User: fnell Date: 2013/04/26 Time: 3:20 PM To
 * change this template use File | Settings | File Templates.
 */
public class Enums {

    public enum BrowserType {

        IE, FireFox, Chrome, Safari
    }

    public enum ResultStatus {

        PASS, FAIL, WARNING, UNCERTAIN
    }

    public enum RelativePosition {

        Above, Below, Right, Left
    }
    
    public enum MobilePlatform {
        Android, iOS
    }
    
    public enum Device 
    {
       Samsung_Tablet("udid", "52004947bca12100", "127.0.0.1", "4495", MobilePlatform.Android),
        geny_motion_Tablet("udid", "192.168.188.101:5555", "127.0.0.1", "4495", MobilePlatform.Android),
        Samsung_Note_2_Tablet("udid", "c3206a701e5aa11", "127.0.0.1", "4495", MobilePlatform.Android),
        Samsung_Note_2("udid", "4df090ae6af88fc1", "127.0.0.1", "4491", MobilePlatform.Android),
        SamSung_S3("udid", "4df0b16c57519fa5", "127.0.0.1", "4492", MobilePlatform.Android),
        SamSung_S3_JP("udid", "4df0ec687dfeafd5", "127.0.0.1", "4493", MobilePlatform.Android),
        SamSung_S4("udid", "4d00cb02c5ce4001", "127.0.0.1", "4493", MobilePlatform.Android),
        Nexus_7("udid", "0921880b", "127.0.0.1", "4495", MobilePlatform.Android),
        //SamSung_S4_FN("udid", "4d00c1b248eb60bd", "http://127.0.0.1:4492/wd/hub", MobilePlatform.Android),
        Iphone4_Hub1("udid", "FC701A3EE383A8A98CDFC3FEB17AB3D55ADB6251", "http://10.0.1.97", "4724", MobilePlatform.iOS),
        SamSung_S4_Rio("udid", "4d00168240c03049", "127.0.0.1", "4723", MobilePlatform.Android),
        Iphone4_Hub2("udid", "069750BDDE450E9E1A8483347C2FD1B8CAABAC0C", "http://10.0.1.97", "4726", MobilePlatform.iOS),
        SamSung_Custom_Phone("udid", "emulator-5554", "127.0.0.1", "4723", MobilePlatform.Android),
        Emulator("udid", "emulator-5554", "127.0.0.1", "4723", MobilePlatform.Android),
        Nexus_5x("udid", "010a06be225db397", "127.0.0.1", "4722", MobilePlatform.Android),
        //Test
        JenkinsSamsung("udid", "836f4c39384e3137", "127.0.0.1", "", MobilePlatform.Android),
        JenkinsBluestacks("udid", "emulator-5554", "127.0.0.1", "", MobilePlatform.Android),
        //Momentum Devices
        iPhone_6("udid", "871bcf2677f6c0460be47b70353cc91c1b476510", "http://localhost", "4723/wd/hub", MobilePlatform.iOS),
        iPhone_7("udid", "ad38fd9c4354dc6b917e106175da10e7bbc936f0", "http://localhost", "4723/wd/hub", MobilePlatform.iOS),
        Samsung_S6_Flat("udid", "0816081d9fc60505", "127.0.0.1", "", MobilePlatform.Android),
        Samsung_S7_Edge("udid", "ad08160338b35f4a41", "127.0.0.1", "", MobilePlatform.Android),
        SamsungTabA("udid", "please add", "127.0.0.1", "", MobilePlatform.Android), //Android1
        SamsungTab4("udid", "please add", "127.0.0.1", "", MobilePlatform.Android), //Android2
        Android3("udid", "device to be determined", "127.0.0.1", "", MobilePlatform.Android),
        IpadMini("udid", "730a7f456e5726120dcd54c2fb9999e279c0b6b6", "http://127.0.0.1", "4723", MobilePlatform.iOS), //IOS1
        IOS2("udid", "device to be determined", "127.0.0.1", "", MobilePlatform.iOS);
       
       public final String CapabilityName;
       public final String DeviceID;
       public final String ServerURL;
       public final String ServerPort;
       public final MobilePlatform platform;
        
       // This constructor defines and instantiates the parameters declared above. Parameter order is specified here and will 
       // define the order in which the enum types' properties are specified. 
       Device(String CapabilityName, String DeviceID, String ServerURL, String ServerPort, MobilePlatform _platform)
       {
           this.CapabilityName = CapabilityName;
           this.DeviceID = DeviceID;
           this.ServerURL = ServerURL;
           this.ServerPort = ServerPort;
           this.platform = _platform;
       }
      
    }
    
    public enum DeviceConfig {
        
        //Capability information is stored here.
        //AppName and AppFilePath are for both APK and IPA.
        //Uses the format Name(deviceName, platformName, automationName, Version, appPackage, appActivity, appName, appFilePath)
        
        Eribank("5554", "Android", "Appium", "4.4", "com.experitest.ExperiBank", ".LoginActivity", "EriBank.apk", System.getProperty("user.dir") + "\\Applications"),
        Test("deviceName", "platformName", "automationName", "Version", "appPackage", "appActivity", "ApplicationName", "ApplicationFIlePath");
        
        public final String deviceName;
        public final String platformName;
        public final String automationName;
        public final String Version;
        public final String appPackage;
        public final String appActivity;
        public final String ApplicationName;
        public final String ApplicationFilePath;
       
        
        // This constructor defines and instantiates the parameters declared above. Parameter order is specified here and will 
       // define the order in which the enum types' properties are specified. 
        
        DeviceConfig(String deviceName, String platformName, String automationName, String Version, String appPackage, String appActivity, String ApplicationName, String ApplicationFilePath)
        {
           this.deviceName = deviceName; 
           this.platformName = platformName; 
           this.automationName = automationName; 
           this.Version = Version; 
           this.appPackage = appPackage; 
           this.appActivity = appActivity; 
           this.ApplicationName = ApplicationName;
           this.ApplicationFilePath = ApplicationFilePath;
        }
    }
    
    public enum Database
    {
        // Set Database Connection Information Here. 
        Example("org.apache.derby.jdbc.EmbeddedDriver","jdbc:derby://localhost:1527/sample","app","app");
        
        public final String Driver;
        public final String ConnectionString;
        public final String username;
        public final String password;
        
        Database(String Driver, String ConnectionString, String username, String password)
        {
            this.Driver = Driver;
            this.ConnectionString = ConnectionString;
            this.username = username;
            this.password = password;
        }
                
    }

    public enum Environment {
        // Add environment urls here, parameter order is defined by the constructor (Environment) definition below
        // Please note that adding an addtional environment type will require you to comma-seperate them.
        // Visit http://docs.oracle.com/javase/tutorial/java/javaOO/enum.html to learn more about Java Enum declarations. 

        // Here we are declaring the Dev Environment type, and passing the following two properties, a url and a connection string, 
        // which are defined below as both being string literals:
        // DEV[FirstPageURL,FirstDatabaseConnectionString]
        
        TEST("https://www.gmail.com/");
        // For each system (website1, database1, website2 etc.) within the defined environment (Dev, QA, Prod etc.)
        // you will have to declare the appropriate string to store its properties (URL or connection string etc.).
        public final String PageUrl;

//        public final String ForgotPasswordURL;
        // This constructor defines and instantiates the parameters declared above. Parameter order is specified here and will 
        // define the order in which the enum types' properties are specified. 
        Environment(String pageUrl) {
            this.PageUrl = pageUrl;
        }

    }
    
    public static Device resolveDevice (String device)
    {
        switch (device.toUpperCase()) {
            case "SAMSUNG_TABLET":
                return  Device.Samsung_Tablet;
            case "SAMSUNG_CUSTOM_PHONE":
               return Device.SamSung_Custom_Phone;
            default:
                return null;
        }
    }
    
    public static DeviceConfig resolveDeviceConfig (String deviceConfig)
    {
        switch (deviceConfig.toUpperCase()) {
            case "ERIBANK":
                return  DeviceConfig.Eribank;
            case "TEST":
                return DeviceConfig.Test;
            default:
                err.println("[WARNING!!!] No matching Device Configuration was found in Enums - switching to default");
                return null;
        }
    }
    
//    public static DeviceConfig resolveDeviceConfig(String deviceConfig)
//    {
//        switch (deviceConfig.toUpperCase()) {
//            case "HOLLARDANDROID":
//                return DeviceConfig.HollardAndroid;
//            
//            default:
//                return null;
//        }
//    }
    
    public static Environment resolveTestEnvironment(String environment) {
        switch (environment.toUpperCase()) {
            case "TEST":
                return Environment.TEST;
            
            default:
            {
                err.println("[WARNING!!!] No matching environment was found in Enums - switching to default");
                return Environment.TEST;
            }
        }
    }
}
