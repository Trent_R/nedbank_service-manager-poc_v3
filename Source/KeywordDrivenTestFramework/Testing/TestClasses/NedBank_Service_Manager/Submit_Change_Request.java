/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.NedBank_POC;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.NedBank_PageObject;

/**
 *
 * @author CC309146
 */
@KeywordAnnotation(
        Keyword = "Submit Change Request",
        createNewBrowserInstance = true
)

public class Submit_Change_Request {

    public TestEntity testData;
    String error = "";
    Narrator narrator;

    public Submit_Change_Request(TestEntity testData) {
        this.testData = testData;
        narrator = new Narrator(testData);
    }

    public TestResult executeTest() {
        
        if (!Open_Change_Description()) {

            return narrator.testFailed("Failed to complete Open Change Description page - " + error);
        }
        if (!Select_Required_Implementation_Dates()) {

            return narrator.testFailed("Failed to complete Select Required Implementation Dates page - " + error);
        }
        if (!Open_Change_Details()) {

            return narrator.testFailed("Failed to complete Open Change Details page - " + error);
        }
        if (!Required_Business_Cluster()) {

            return narrator.testFailed("Failed to complete Required Business Cluster page - " + error);
        }
        if (!Add_Implementor_Approver_Details()) {

            return narrator.testFailed("Failed to complete Add Implementor Approver Details page - " + error);
        }

        if (!Add_Affected_Assets()) {

            return narrator.testFailed("Failed to complete Add Affected Assets/Cis page - " + error);
        }
        if (!Open_Change_Scheduling()) {

            return narrator.testFailed("Failed to complete the Open Change page - " + error);
        }

        return narrator.finalizeTest("Successfully submited Change Request on Nedbank Service Manager.");
    }

    public boolean Open_Change_Description() {
        
        //............ 
        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.SubmitChangeRequestBtn())) {
            error = "Failed to wait for 'Submit General, Urgent, Standard and Emergency Change Request' Nedbank Service Manager page.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.SubmitChangeRequestBtn())) {
            error = "Failed to Click 'Submit General, Urgent, Standard and Emergency Change Request' Nedbank Service Manager page.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.DescriptionofChangeWaitText())) {
            error = "Failed to wait for Description of Change textBox on Nedbank Service Manager Open Change Description page";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.DescriptionofChangeBtn(), testData.getData("DescriptionofChange"))) {
            error = "Failed to enter Description of Change on Nedbank Service Manager Open Change Description page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.BenefitMotivationofChangeClick())) {
            error = "Failed to wait for Benefit Motivation of Change text box on Nedbank Service Manager Open Change Description page";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.BenefitMotivationofChangeBtn(), testData.getData("MotivationofChange"))) {
            error = "Failed to enter Motivation of Change Nedbank Service Manager Open Change Description page";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.PriorityofChangeDropDwn(), testData.getData("PriorityofChange"))) {
            error = "Failed to enter Priority of Change on Nedbank Service Manager Open Change Description page";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.PlatformDropDwn(), testData.getData("Platform"))) {
            error = "Failed to enter Platform on Nedbank Service Manager Open Change Description page";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.CategoryDropDwn(), testData.getData("Category"))) {
            error = "Failed to enter Category on Nedbank Service Manager Open Change Description page";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.EnvironmentDropDwn(), testData.getData("Environment"))) {
            error = "Failed to enter Environment on Nedbank Service Manager Open Change Description page";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.ImplementationSiteDroopDwn(), testData.getData("Implementation"))) {
            error = "Failed to enter Implementation on Nedbank Service Manager Open Change Description page";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.TypeofRequestDropDwn(), testData.getData("TypeofRequest"))) {
            error = "Failed to enter TypeofRequest on Nedbank Service Manager Open Change Description page";
            return false;
        }
        
                narrator.stepPassedWithScreenShot("Successfully entered \"Open Change - Description\" details.");

        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait for Open Change Next textBox on Nedbank Service Manager";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to click Next button on Nedbank Service Manager";
            return false;
        }
        

        return true;
    }

    public boolean Select_Required_Implementation_Dates() {

                                 //........Enter System Start and End Date.......// 
        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.SelectRequiredDatesWait())) {
            error = "Failed to wait for Select Required Dates on Nedbank Service Manager ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.PlannedStartbtn(), 1)) {
            error = "Failed to wait for Nedbank Planned Start date on Nedbank Service Manager ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.PlannedStartbtn())) {
            error = "Failed to click Nedbank Planned Start date on Nedbank Service Manager ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.StartDateBtn())) {
            error = "Failed to select start date on Nedbank Service Manager ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.StartDateOK())) {
            error = "Failed to click Ok for Start date on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.PlanedEndDateBtn())) {
            error = "Failed to click Nedbank Planned End date on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.EndDateBtn())) {
            error = "Failed to select Nedbank Planned End date on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.StartDateOK())) {
            error = "Failed to click Ok for Start date on Nedbank Service Manager";
            return false;
        }
        
                                //......... Select Schedule Down Time.......
                                
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.ScheduleDowntimeBtn())) {
            error = "Failed to click Schedule Downtime on Nedbank Service Manager";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered \"Required Implementation Dates\" details.");
            
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait for Open Change Next button on Nedbank Service Manager";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to click Open Change Next button on Nedbank Service Manager ";
            return false;
        }

        return true;
    }

    public boolean Open_Change_Details() {

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDetailsWait())) {
            error = "Failed to wait for Open Change Details page";
            return false;
        }
            //./........
            
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.IMPLEMENTING_EXEC(), testData.getData("IMPLEMENTINGEXEC"))) {
            error = "Failed to enter text IMPLEMENTINGEXEC into Textbox";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.REQUESTING_EXEC(), testData.getData("REQUESTINGEXEC"))) {
            error = "Failed to enter text into REQUESTINGEXEC Textbox";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.CHANGE_OWNER_EXEC(), testData.getData("CHANGEOWNEREXEC"))) {
            error = "Failed to enter text CHANGEOWNEREXEC into Textbox";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.SYSTEM_OWNER_EXEC(), testData.getData("SERVICESYSTEMOWNEREXEC"))) {
            error = "Failed to enter text SERVICESYSTEMOWNEREXEC into Textbox";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.Change_Purpose(), testData.getData("ChangePurpose"))) {
            error = "Failed to enter text into Change Purpose Textbox";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.Risk_Level(), testData.getData("RiskLevel"))) {
            error = "Failed to enter text into Risk Level Textbox";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.Impact(), testData.getData("Impact"))) {
            error = "Failed to enter text into Impact Textbox";
            return false;
        }
        
            narrator.stepPassedWithScreenShot("Successfully entered \"Open Change - EXEC\" details.");


        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to click Open Change Next button on Nedbank Service Manager";
            return false;
        }

        return true;
    }

    public boolean Required_Business_Cluster() {

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Required_Business_ClusterWait())) {
            error = "Failed to waite for Required Business Cluster page on Nedbank Service Manager ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.ADMCheckBox())) {
            error = "Failed to click ADM Check Box Nedbank Service Manager";
            return false;
        }
        
           narrator.stepPassedWithScreenShot("Successfully selected \"Required Business Cluster's\".");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to click Open Change Next button on Nedbank Service Manager";
            return false;
        }

        return true;
    }

    public boolean Add_Implementor_Approver_Details() {

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Implementor_Approver_DetailsPage())) {
            error = "Failed to wait for Add Implementor Approver Details page on  Nedbank Service Manager";
            return false;
        }
    
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.Change_Implementer(), testData.getData("EmployeeNoCCNB"))) {
            error = "Failed to enter text Change Implementer on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Search_Implementer())) {
            error = "Failed to wait for Search button for Implementeron on Nedbank Service Manager ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Search_Implementer())) {
            error = "Failed to wait for Search button for Implementeron on Nedbank Service Manager";
            return false;
        }
        //Validate Implementor after search 
        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Validate_searched_Implementor())) {
            error = "Failed to Validate searched Implementor ";
            return false;
        }
        


        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Add_Required_ApproverBtn())) {
            error = "Failed to waite for Add Required Approver button on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Add_Required_ApproverBtn())) {
            error = "Failed to click Add Required Approver button on Nedbank Service Manager";
            return false;
        }

        //...................................Please Enter an Employee CC/NB and/or Group Name......
        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Employee_CC_NB_GroupPage())) {
            error = "Failed to wait for Employee CC/NB Group Page on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Employee_CC_NB())) {
            error = "Failed to wait for Employee CC/NB textbox on Nedbank Service Manager";
            return false;
        }
 
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.Employee_CC_NB(), testData.getData("EmployeeNoCCNB"))) {
            error = "Failed to enter Employee CC/NB text on Nedbank Service Manager ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.CC_NB_Search())) {
            error = "Failed to wait for the Employee CC/NB Search button on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.CC_NB_Search())) {
            error = "Failed to click Employee CC/NB Search button on Nedbank Service Manager";
            return false;
        }

        //.........  ..........................  Employee CC/NB number  ...........         
        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch to Employee CC/NB number default Iframe on Nedbank Service Manager page.";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Approval_Pop_Up())) {
            String Approval_PopUp_Message = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Approval_Text(testData.getData("EmployeeNoCCNB")));
            testData.extractParameter("CC/NB Approval", Approval_PopUp_Message, "pass");
        } else {
            error = "Failed to wait and validate Approval pop Up text for Employee CC/NB on Nedbank Service Manager ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Approval_Okbtn())) {
            error = "Failed to click Approval Ok button on Nedbank Service Manager ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Approval_Message())) {

            String Approval_Text = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Approval_Message_Text());

            //......VAlidate Approval text from the Approval_popUp Validate if its the same on the next page popUp
 
            String confirmText = testData.getExtractedParameter("CC/NB Approval");

            if (confirmText.contains(Approval_Text)) {
                testData.extractParameter("Approval Employee CC/NB", Approval_Text, "pass");
            }
        } else {
            String confirmText = testData.getExtractedParameter("Approval");
            error = "Failed to find Employee Approval Number " + confirmText + "on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(NedBank_PageObject.Required_ImplementerDeafault_Frame())) //iframe[contains(@title, 'Wizard')]"))
        {
            error = "Failed to switch to the Add Required Implementer Employee CC/NB Default Iframe on Nedbank Service Manager page.";
            return false;
        }

//            narrator.stepPassedWithScreenShot("Adding Employee CC/NB Step passed");

        
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to click Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        //.........Add Select Group NAme........
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Add_Required_ApproverBtn())) {
            error = "Failed to wait for Add Select Group Name page on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Add_Required_ApproverBtn())) {
            error = "Failed to to click Add Required Approver button on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Group_Name())) {
            error = "Failed to wait for Group Name textBox on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.Group_Name(), testData.getData("GroupName"))) {
            error = "Failed to enter Group Name text on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Group_Search())) {
            error = "Failed to wait for Group Search button on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Group_Search())) {
            error = "Failed to click Group Search button on Nedbank Service Manager";
            return false;
        }

        //.........Group Name Approval validation     
        
        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch to Group Name default iframe on Nedbank Service Manager page.";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Approval_Pop_Up())) {
            String Approval_PopUp_Message = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Approval_Text(testData.getData("GroupName")));
            testData.extractParameter("Group Approval", Approval_PopUp_Message, "pass");
        } else {
            error = "Failed to wait and validate Approval pop Up text for Group Name on Nedbank Service Manager";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Approval_Okbtn())) {
            error = "Failed to click Approval Ok button on Nedbank Service Manager";
            return false;
        }

                    //.......Validate if the Approval Message is the same as the Approval PopUp  alert ......

        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Approval_Message()))
        {
            String Approval_Message = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Approval_Message_Text());
            String confirmText = testData.getExtractedParameter("Group Approval");
            
            if (confirmText.contains(Approval_Message)) 
            {
                testData.extractParameter("Group Approval", Approval_Message, "pass");
            }
            else
            {
                error = "Failed to validate expected message: " + confirmText;
                return false;
            }
        }
        else 
        {
            String confirmText = testData.getExtractedParameter("Group Approval");
            error = "Failed to find Group Approval Name " + confirmText + "on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(NedBank_PageObject.Required_ImplementerDeafault_Frame())) 
        {
            error = "Failed to switch to the Add Required Implementer Group Approval Default Iframe on Nedbank Service Manager page.";
            return false;
        }
        
//            narrator.stepPassedWithScreenShot("Adding Group Name Step passed");

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait for Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to click Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        //.......SEARCH CONTACT INFORMATION
        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Add_Required_ApproverBtn())) {
            error = "Failed to wait for SEARCH CONTACT INFORMATION page on Nedbank Service Manager ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Add_Required_ApproverBtn())) {
            error = "Failed to to click Add Required Approver button on Nedbank Service Manager";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.CC_NB_Search())) {
            error = "Failed to waite for SEARCH CONTACT button on Nedbank Service Manager  ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.CC_NB_Search())) {
            error = "Failed to Failed to waite for SEARCH CONTACT button on Nedbank Service Manager ";
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch to the default Iframe on Nedbank Service Manager page.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(NedBank_PageObject.ContactInfoIframe())) 
        {
            error = "Failed to switch to ContactInfo Iframe on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.last_name_Search())) {
            error = "Failed to wait for Last name text box on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.last_name_Search(), testData.getData("EmployeeSearchName"))) {
            error = "Failed to enter Employee Last Name into  textBox on on Nedbank Service Manager page";
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to default frame on Nedbank Service Manager page.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Search_Btn())) {
            error = "Failed to wait for search button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Search_Btn())) {
            error = "Failed to click search button on Nedbank Service Manager page";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(NedBank_PageObject.SearchPopUpFarme())) {
            error = "Failed to switch Iframe on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.EmployeeID_Wait())) {
            error = "Failed to wait for Employee ID on Nedbank Service Manager page";
            return false;
        }
        
//         narrator.stepPassedWithScreenShot("Step passed");

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Search_Contact_Select())) {
            error = "Failed to click on the Searched Contact ID on Nedbank Service Manager page";
            return false;
        }
        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch default iframe on Nedbank Service Manager page.";
            return false;
        }
                
        //.... Validate if the searched Implementer is found on the System....
        
        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Approval_Pop_Up())) {
            String Approval_PopUp_Message = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Approval_Text(testData.getData("ChangeImplementer")));
            testData.extractParameter("Change Implementer Approval", Approval_PopUp_Message, "pass");
        } else {
            error = "Failed to wait and validate Approval pop Up text for Change Implementer on Nedbank Service Manager";
            return false;
        }
        

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Approval_Okbtn())) {
            error = "Failed to click Approval Ok button on Nedbank Service Manager";
            return false;
        }

                    //.......Validate if the Approval Message is the same as the Approval PopUp  alert 

        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Approval_Message())) {

            String Approval_Message = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Approval_Message_Text());
            String confirmText = testData.getExtractedParameter("Change Implementer Approval");

            
            if (confirmText.contains(Approval_Message)) {
                testData.extractParameter("Change Implementer Approval", Approval_Message, "pass");
            }
            else
            {
                error = "Failed to validate expected message: " + confirmText;
                return false;
            }

        } else {
            String Conferm = testData.getExtractedParameter("Change Implementer Approval");
            error = "Failed to find Group Approval Name " + Conferm + "on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(NedBank_PageObject.Required_ImplementerDeafault_Frame())) 
        {
            error = "Failed to switch Iframe on Nedbank Service Manager page.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to click Open Change Next button on Nedbank Service Manager page";
            return false;
        }
        //......... Validation Add Required Approvers Table after all the transactions ..........................           
        try {
            if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Implementor_Approver_DetailsPage())) {
                String Approval_Table = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Add_Required_Approvers_table());
                testData.extractParameter("Add Required Approvers Table", Approval_Table, "pass");

            }
        } catch (Exception e) 
        {
            error = "Failed to validate Add Required Approvers table";
            return false;
        }

             narrator.stepPassedWithScreenShot("Successfully added required Implementor/Approver Details.");

        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Add_Reviewer_Details_Page())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        
        return true;

    }

    public boolean Add_Affected_Assets() {
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Add_Affected_AssetsPage())) {
            error = "Failed to wait for the Add Affected Assets Page on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.System_Application_Name())) {
            error = "Failed to wait for System Application Name textBox on Nedbank Service Manager page";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.System_Application_Name(), testData.getData("SystemApplicationName"))) {
            error = "Failed to enterText into System Application Name textBox on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Application_Name_DropDwn())) {
            error = "Failed to select System Application Name from drop down list on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.No_of_Components())) {
            error = "Failed to wait for Total No Of Components textBox on Nedbank Service Manager page";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.No_of_Components(), testData.getData("TotalNoOfComponents"))) {
            error = "Failed to enterText into Total No Of Components textBox on Nedbank Service Manager page";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully added Affected Assets.");

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }
        
        return true;
    }

    public boolean Open_Change_Scheduling() {
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Open_ChangePage())) {
            error = "Failed to wait for the Open Cahange Page on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Scheduling_changes())) {
            error = "Failed to wait for Scheduling Changes/JCL Changes TextBox on  Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.Scheduling_changes(), testData.getData("SchedulingChanges"))) {
            error = "Failed to select from Scheduling Changes/JCL Changes Dropdown on  Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.authorize_form())) {
            error = "Failed to wait for File Authorization Form TextBox on  Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.authorize_form(), testData.getData("AuthorizeForm"))) {
            error = "Failed to select from File Authorization Form Dropdown on  Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.D2D_changes())) {
            error = "Failed to wait for DB2 Changes TextBox on Nedbank Service Manager page ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.D2D_changes(), testData.getData("DB2Changes"))) {
            error = "Failed to select from DB2 Changes  Dropdown on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Cics_Change())) {
            error = "Failed to wait for Cics Changes TextBox on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.Cics_Change(), testData.getData("CicsChange"))) {
            error = "Failed to select from Cics Changes Dropdown on Nedbank Service Manager pag";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered \"Open Change - Scheduling\" details.");

        
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.OpenChangeDesNextBtn())) {
            error = "Failed to wait Open Change Next button on Nedbank Service Manager page";
            return false;
        }

        //..............Validate Change request submit document page................
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Submition_page())) {
            error = "Failed to wait for the Submit you document page on Nedbank Service Manager page";
            return false;
        }

        if (SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Submit_Message())) {
            String Submit_Message = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Submit_Message());
            testData.extractParameter("Submit final Message", Submit_Message, "pass");
        }        
        
//        narrator.stepPassedWithScreenShot("Change request submit document page Step passed");

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Finsh_Button())) {
            error = "Failed to wait for the Finsh Button on Nedbank Service Manager page";
            return false;
        }

        return true;
    }

}
