/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.NedBank_POC;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SikuliDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.NedBank_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.sikuli.script.Key;

/**
 *
 * @author CC309146
 */
@KeywordAnnotation(
        Keyword = "Navigate to Change Request",
        createNewBrowserInstance = true
)

public class NavigateToChangeProcess {

    public TestEntity testData;
    String error = "";
    Narrator narrator;

    public NavigateToChangeProcess(TestEntity testData) {
        this.testData = testData;
        narrator = new Narrator(testData);
        SikuliDriverInstance = new SikuliDriverUtility("");
    }

    public TestResult executeTest() {
        if (!NavigateToChangeRequest()) {
            return narrator.testFailed("Failed to navigate to Nedbank service manager Change Request page - " + error);
        }

        return narrator.finalizeTest("Successfully navigated to Nedbank service manager Change Request page");
    }

    public boolean NavigateToChangeRequest() {
                    
                //Navigate To Change Request from the Service Manager home page
         
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.ChangeManagementBtn())) {
            error = "Failed to click  Change management button on Nedbank Service Manager home page.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.CreateNewChangeBtn())) {
            error = "Failed to click Create New Change button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByXpath(NedBank_PageObject.Service_ManagerFrame1()))
        {
            error = "Failed to switch Iframe on Nedbank Service Manager page.";
            return false;   
        }
        
        narrator.stepPassedWithScreenShot("Navigate To Change Request page-");
       
                //Selecting the transaction you would like to change...
                
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.CHANGE_REQUESTBtn()))
        {
             error = "Failed to click the CHANGE REQUESTBtn button on Nedbank Service Manager page.";
            return false;  
        }
                
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.RequiredOption())) 
        {
            error = "Failed to wait for Please Select Required Options page on Nedbank Service Manager page.";
            return false;
        } 
        
        return true;
    }

}
