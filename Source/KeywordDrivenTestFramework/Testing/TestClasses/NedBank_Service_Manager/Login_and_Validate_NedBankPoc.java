/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.NedBank_POC;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.NedBank_PageObject;
import org.sikuli.script.Key;

/**
 *
 * @author CC309146
 */
@KeywordAnnotation(
        Keyword = "LogIn To Nedbank Service Manager",
        createNewBrowserInstance = true
)

public class Login_and_Validate_NedBankPoc extends BaseClass {

    public TestEntity testData;
    String error = "";
    Narrator narrator;

    public Login_and_Validate_NedBankPoc(TestEntity testData) {
        this.testData = testData;
        narrator = new Narrator(testData);

    }

    public TestResult executeTest() {
        
        // This step will Launch the browser and navigate to the Nedbank Service Manager URL
        if (!LogInandValidate()) {
            return narrator.testFailed("Failed to navigate to Nedbank Service Manager page - " + error);
        }
        return narrator.finalizeTest("Successfully navigated, Logged In and Validated the Nedbank Service Manager Page");
    }

    public boolean LogInandValidate() {

        if (!SeleniumDriverInstance.navigateTo(NedBank_PageObject.ServiceManagerURL())) {
            error = "Failed to navigate to Nedbank Service Manager URL.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.ServiceManagerUsername(), testData.getData("Username"))) {
            error = "Failed to enter client username.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.ServiceManagerPassword(), testData.getData("Password"))) {
            error = "Failed to enter client Password.";
            return false;
        }
            narrator.stepPassedWithScreenShot("Username and Password Step passed.");

        
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.ServiceManagerloginBtn())) {
            error = "Failed to click Service Manager Login Button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.ChangeManagementBtn())) {
            error = "Failed to waite for Change management button on Service Manager home page.";
            return false;
        }
        
        
        return true;
    }
}
