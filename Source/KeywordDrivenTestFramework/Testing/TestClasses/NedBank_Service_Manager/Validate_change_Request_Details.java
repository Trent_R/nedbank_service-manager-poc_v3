/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.NedBank_POC;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.NedBank_PageObject;

/**
 *
 * @author CC309146
 */
@KeywordAnnotation(
        Keyword = "Validate Change Request",
        createNewBrowserInstance = true
)
public class Validate_change_Request_Details {

    public TestEntity testData;
    String error = "";
    Narrator narrator;
    String Change_Approval;

    public Validate_change_Request_Details(TestEntity testData) {
        this.testData = testData;
        narrator = new Narrator(testData);
    }

    public TestResult executeTest() {
        if (!NedBank_Validation()) {

            return narrator.testFailed("Failed to complete NedBank Validation for Change Management Manual - " + error);
        }

        if (!CM_CheckList()) {
            return narrator.testFailed("Failed to complete NedBank CM CheckList Page- " + error);
        }

        if (!Finalise()) {
            return narrator.testFailed("Failed to Finalise - " + error);
        }

        return narrator.finalizeTest("Successfully created new change. Message: " + Change_Approval);
    }

    public boolean NedBank_Validation() {
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Finsh_Button())) {
            error = "Failed to click the Finsh Button on Nedbank Service Manager page ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Finsh_Button())) {
            error = "Failed to click the Finsh Button on Nedbank Service Manager page ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Description_of_Change())) {
            error = "Failed to validate Description of Change on Nedbank Service Manager page";
            return false;
        } else {
            String Description_of_Change = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Description_of_Change());
            if (Description_of_Change.contains(testData.getData("DescriptionofChange"))) {
                testData.extractParameter("Description of Change", Description_of_Change, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("DescriptionofChange") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Motivation_of_Change())) {
            error = "Failed to validate Motivation of Change on Nedbank Service Manager page";
            return false;
        } else {
            String Motivation_of_Change = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Motivation_of_Change());
            testData.extractParameter("Motivation of Change validation", Motivation_of_Change, "pass");
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Priority_of_Change())) {
            error = "Failed to validate Priority of Change on Nedbank Service Manager page";
            return false;
        } else {
            String Priority_of_Change = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Priority_of_Change());
            if (Priority_of_Change.contains(testData.getData("PriorityofChange"))) {
                testData.extractParameter("Priority of Change", Priority_of_Change, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("PriorityofChange") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Type_of_Request())) {
            error = "Failed to validate Type of Request on Nedbank Service Manager page";
            return false;
        } else {
            String Type_of_Request = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Type_of_Request());
            if (Type_of_Request.contains(testData.getData("TypeofRequest"))) {
                testData.extractParameter("Type of Request", Type_of_Request, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("TypeofRequest") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Planned_Start())) {
            error = "Failed to validate Planned Start date on Nedbank Service Manager page ";
            return false;
        } else {
            String Planned_Start = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Planned_Start());
            if (Planned_Start.contains(testData.getData("PlannedStartDate"))) {
                testData.extractParameter("Planned Start date", Planned_Start, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("PlannedStartDate") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Planned_End())) {
            error = "Failed to validate Planned End date on Nedbank Service Manager page";
            return false;
        } 
        else
        {
            String Planned_End = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Planned_End());
            if (Planned_End.contains(testData.getData("PlannedEndDate"))) {
                testData.extractParameter("Planned End Date", Planned_End, "pass");
            } else if (Planned_End.equals("")) {
                error = "Failed to extract text by VAlue for Planned End";
                return false;
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("PlannedEndDate") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Change_Implementor_CCNB())) {
            error = "Failed to validate Change Implementor CC/NB number on Nedbank Service Manager page";
            return false;
        } else {
            String Change_Implementor_CCNB = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Change_Implementor_CCNB());
            if (Change_Implementor_CCNB.contains(testData.getData("EmployeeNoCCNB"))) {
                testData.extractParameter("Employee CC/NB number", Change_Implementor_CCNB, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("EmployeeNoCCNB") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Employee_Full_Name())) {
            error = "Failed to validate Employee Full Name on Nedbank Service Manager page";
            return false;
        } else {
            String Employee_Full_Name = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Employee_Full_Name());
            if (Employee_Full_Name.contains(testData.getData("EmployeeFullName"))) {
                testData.extractParameter("Employee Full Name", Employee_Full_Name, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("EmployeeFullName") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Risk_Level_Value())) {
            error = "Failed to validate Risk Level on Nedbank Service Manager page ";
            return false;
        } else {
            String Risk_Level_Value = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Risk_Level_Value());
            if (Risk_Level_Value.contains(testData.getData("RiskLevel"))) {
                testData.extractParameter("Risk Level", Risk_Level_Value, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("RiskLevel") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Impact_Value())) {
            error = "Failed to validate Impact on Nedbank Service Manager page";
            return false;
        } else {
            String Impact_Value = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Impact_Value());
            if (Impact_Value.contains(testData.getData("Impact"))) {
                testData.extractParameter("Impact ", Impact_Value, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("Impact") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Change_Purpose_value())) {
            error = "Failed to validate Change Purpose on Nedbank Service Manager page ";
            return false;
        } else {
            String Change_Purpose_value = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Change_Purpose_value());
            if (Change_Purpose_value.contains(testData.getData("ChangePurpose"))) {
                testData.extractParameter("Change Purpose", Change_Purpose_value, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("ChangePurpose") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Platform())) {
            error = "Failed to validate Platform on Nedbank Service Manager page";
            return false;
        } else {
            String Platform = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Platform());
            if (Platform.contains(testData.getData("Platform"))) {
                testData.extractParameter("Platform", Platform, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("Platform") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Environment())) {
            error = "Failed to validate Environment on Nedbank Service Manager page";
            return false;
        } else {
            String Environment = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Environment());
            if (Environment.contains(testData.getData("Environment"))) {
                testData.extractParameter("Environment", Environment, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("Environment") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Implementation_Site())) {
            error = "Failed to validate Implementation Site on Nedbank Service Manager page";
            return false;
        } else {
            String Implementation_Site = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Implementation_Site());
            if (Implementation_Site.contains(testData.getData("Implementation"))) {
                testData.extractParameter("Implementation site", Implementation_Site, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("Implementation") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Category())) {
            error = "Failed to validate Category on Nedbank Service Manager page";
            return false;
        } else {
            String Category = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.Category());
            if (Category.contains(testData.getData("Category"))) {
                testData.extractParameter("Implementation site", Category, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("Category") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.IMPLEMENTING_Name())) {
            error = "Failed to validate IMPLEMENTING NAME on Nedbank Service Manager page";
            return false;
        } else {
            String IMPLEMENTING_Name = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.IMPLEMENTING_Name());
            if (IMPLEMENTING_Name.contains(testData.getData("IMPLEMENTINGEXEC"))) {
                testData.extractParameter("IMPLEMENTING Name", IMPLEMENTING_Name, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("IMPLEMENTINGEXEC") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.REQUESTING_Name())) {
            error = "Failed to validate REQUESTING NAME on Nedbank Service Manager page";
            return false;
        } else {
            String REQUESTING_Name = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.REQUESTING_Name());
            if (REQUESTING_Name.contains(testData.getData("REQUESTINGEXEC"))) {
                testData.extractParameter("REQUESTING Name", REQUESTING_Name, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("REQUESTINGEXEC") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.CHANGE_OWNER_Name())) {
            error = "Failed to validate CHANGE OWNER NANE on Nedbank Service Manager page";
            return false;
        } else {
            String CHANGE_OWNER_Name = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.CHANGE_OWNER_Name());
            if (CHANGE_OWNER_Name.contains(testData.getData("CHANGEOWNEREXEC"))) {
                testData.extractParameter("CHANGE OWNER Name", CHANGE_OWNER_Name, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("CHANGEOWNEREXEC") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.ERVICE_SYSTEM_Name())) {
            error = "Failed to validate ERVICE SYSTEM NAME on Nedbank Service Manager page";
            return false;
        } else {
            String ERVICE_SYSTEM_Name = SeleniumDriverInstance.retrieveTextfromValueByXpath_AndValidate(NedBank_PageObject.ERVICE_SYSTEM_Name());
            if (ERVICE_SYSTEM_Name.contains(testData.getData("SERVICESYSTEMOWNEREXEC"))) {
                testData.extractParameter("ERVICE SYSTEM Name", ERVICE_SYSTEM_Name, "pass");
            } else {
                error = "Failed to validate retrieved Text aginst expected value" + testData.getData("SERVICESYSTEMOWNEREXEC") + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Clusters())) {
            error = "Failed to validate Clusters on Nedbank Service Manager page ";
            return false;
        } else {
            String Clusters = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Clusters());
            testData.extractParameter("Clusters", Clusters, "pass");
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.CM_CheckListBTN())) {
            error = "Failed to wait for the CheckList Button on Nedbank Service Manager page";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validated the expected data on the \"Create New Change\" screen.");
        return true;
    }

    public boolean CM_CheckList() {
        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.CM_CheckListBTN())) {
            error = "Failed to Click the the CheckList Button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Update_CM_ChecklistBtn())) {
            error = "Failed to wait for the click to View and Update CM Checklist Button on Nedbank Service Manager page";
            return false;
        }
//        narrator.stepPassedWithScreenShot("navigation to Click to View and Update CM Checklist page Step passed");

        String HomeTab = SeleniumDriverInstance.Driver.getWindowHandle();

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Update_CM_ChecklistBtn())) {
            error = "Failed to click the Click to View and Update CM Checklist Button on Nedbank Service Manager page";
            return false;
        }
        for (String CM_CheckList_Tab : SeleniumDriverInstance.Driver.getWindowHandles()) {

            SeleniumDriverInstance.Driver.switchTo().window(CM_CheckList_Tab);
        }

        //..............nedCMChecklist page 
//        narrator.stepPassedWithScreenShot("navigation to CM Checklist Questions page Step passed");

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.CMCheckListOkBtn())) {
            error = "Failed to wait for the CM Checklist Page on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch To Default Content on the nedCMChecklist page";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(NedBank_PageObject.nedCMChecklistFrame())) {
            error = "Failed to switch To nedCMChecklist IFrame";
            return false;
        }

//        narrator.stepPassedWithScreenShot("CM_Checklist Questions page Step passed");

        //fill in question answers
        for (int i = 1; i < 20; i++) {
            if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.CM_CheckList_QuestionsAndNum(i))) {
                error = "Failed to wait for CM CheckList Questions number-" + i + "";
                return false;
            }
            if (i == 4) {
                if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.CM_CheckList_QuestionsAndNum(4), "No")) {
                    error = "Failed to enter answer for CM CheckList Questions number-" + i + "";
                    return false;
                }

            } else if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.CM_CheckList_QuestionsAndNum(i), "Yes")) {
                error = "Failed to enter answer for CM CheckList Questions number-" + i + "";
                return false;
            }
        }

        //fill in required "no" comments
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.nedCMChecklistComments())) {
            error = "Failed to wait for CM CheckList comments TextBox- ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.nedCMChecklistComments(4), "Filed is Requierd to be No")) {
            error = "Failed to enter CM CheckList comments for Questions munber-" + 4 + "";
            return false;
        }

        //complete questions
        for (int i = 6; i < 8; i++) {
            if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.nedCMChecklistComments(i))) {
                error = "Failed to wait for CM CheckList comments TextBox for Questions munber-" + i + "";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(NedBank_PageObject.nedCMChecklistComments(i), "Test")) {
                error = "Failed to enter CM CheckList comments for Questions munber-" + i + "";
                return false;
            }
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch To Default Content on the nedCMChecklist page";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully completed \"CM Checklist\" question form.");

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.CMCheckListOkBtn())) {
            error = "Failed to wait for the CM Checklist Ok button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.CMCheckListOkBtn())) {
            error = "Failed to click OK for CM Checklist- Page on Nedbank Service Manager page";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.nedCMChecklistAprovalPopUp())) {
            error = "Failed to wait for the CM Checklist nedCMChecklist Aproval message on Nedbank Service Manager page";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.CMChecklistAprovalYesBtn())) {
            error = "Failed to wait for the CM Checklist Aproval message Yes button on Nedbank Service Manager page";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.CMChecklistAprovalYesBtn())) {
            error = "Failed to click the CM Checklist Aproval message Yes button on Nedbank Service Manager page";
            return false;
        }
        //...........Validating that the CM check List has been Upadated........... 
        SeleniumDriverInstance.Driver.switchTo().window(HomeTab);

        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.nedCMChecklistUpadatedText())) {
            String nedCMChecklist_updatedMsg = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.nedCMChecklistUpadatedText());
            if (nedCMChecklist_updatedMsg.contains("nedCMChecklist record updated.")) {
                testData.extractParameter("nedCMChecklist", nedCMChecklist_updatedMsg, "pass");
            } else {
                error = "Failed to wait and validate Approval pop Up text for the NedBank CMChecklist ";
                return false;
            }
        } else {
            error = "Failed to wait and validate Approval pop Up text for the NedBank CMChecklist";
            return false;
        }
        return true;
    }

    public boolean Finalise() {

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.FinaliseSaveBtn())) {
            error = "Failed to wait for Save Button on Nedbank Service Manager Finalise page  .";
            return false;

        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.FinaliseSaveBtn())) {
            error = "Failed to click the Save Button on Nedbank Service Manager Finalise page  .";
            return false;

        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch to Finalise Service Manager default Iframe on Nedbank Service Manager page.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.FinaliseAlert())) {
            error = "Failed to wait for the Finalise Alert on Nedbank Service Manager page.";
            return false;

        }
        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.PopUp_Change_Number())) {
            String Change_Number_Logged = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.PopUp_Change_Number());
            testData.extractParameter("Change Number Logged", Change_Number_Logged, "pass");
        } else {
            error = "Failed to find the Logged Change Number on Nedbank Service Manager page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Approval_Okbtn())) {

            error = "Failed to click the Finalise Alert OK button on Nedbank Service Manager page.";
            return false;

        }
        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Change_confirm_Message())) {
//            narrator.stepPassedWithScreenShot("Finalise Service Manager Step passed");

            String Change_Number = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Change_confirm_Message());
            testData.extractParameter("Change Number added", Change_Number, "pass");
        } else {
            error = "Failed to find the Change Number added alert on Nedbank Service Manager page.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Submit_ChangeBtn())) {
            error = "Failed to wair for the Submit and Change button on Nedbank Service Manager page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(NedBank_PageObject.Submit_ChangeBtn())) {
            error = "Failed to click the Submit and Change button on Nedbank Service Manager page.";
            return false;
        }

        Change_Approval = SeleniumDriverInstance.retrieveTextByXpath(NedBank_PageObject.Change_confirm_Message());
        
        if (SeleniumDriverInstance.waitForElementByXpath(NedBank_PageObject.Change_confirm_Message())) {
//            narrator.stepPassedWithScreenShot("Approval of Change on Service Manager Step passed");
            testData.extractParameter("CHANGE CONTROL Approval", Change_Approval, "pass");
        } else {
            error = "Failed to find CHANGE CONTROL Approval alert on Nedbank Service Manager page.";
            return false;
        }

        return true;
    }

}
