/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.currentEnvironment;

/**
 *
 * @author CC309146
 */
public class NedBank_PageObject {

    public static String ServiceManagerURL() {

        return currentEnvironment.PageUrl;
    }

    public static String ServiceManagerUsername() {
        return "//input[@id='LoginUsername']";
    }

    public static String ServiceManagerPassword() {
        return "//input[@id='LoginPassword']";
    }

    public static String ServiceManagerloginBtn() {
        return "//input[@id='loginBtn']";
    }

    public static String ChangeManagementBtn() {
        return "//div[@id='ROOT/Change Management']//span[contains(text(),'Change Management')]";
    }

    public static String CreateNewChangeBtn() {
        return "//span[contains(text(),'Create New Change')]";
    }

    public static String CHANGE_REQUESTBtn() {
        return //"//a[contains(text(),'CHANGE REQUEST')]//..//..//..//td";
                //div//table//tbody//tr//td//div[@class='x-grid3-cell-inner x-grid3-col-0 x-unselectable ']//a[contains(text(),'CHANGE REQUEST')]";
                //div[@class='x-grid3-scroller']//a[contains(text(),'CHANGE REQUEST')]";
                "//a[contains(text(),'CHANGE REQUEST')]";
    }

    public static String RequiredOption() {
        return "//span[contains(text(),'Please Select Required Option')]";
    }

    public static String SubmitChangeRequestBtn() {
        return //"//button[contains(text(),'Submit General, Urgent, Standard and Emergency Change Request')]//..//..//..//td[2]";
                "//button[contains(text(),'Submit General, Urgent, Standard and Emergency Change Request')]";
    }

    public static String DescriptionofChangeBtn() {
        return "//label[contains(text(),'Description of Change')]/..//..//div/input[@id='X6']";
    }

    public static String DescriptionofChangeWaitText() {
        return "//label[contains(text(),'Description of Change')]";
    }

    public static String BenefitMotivationofChangeBtn() {
        return "//label[contains(text(),'Benefit & Motivation of Change ')]//..//..//div/textarea";
        //label[contains(text(),'Benefit & Motivation of Change ')]//..//..//div[@id='X8View']";
        //label[contains(text(),'Benefit & Motivation of Change ')]//..//..//div[@id='X8Edit']";
    }

    public static String BenefitMotivationofChangeClick() {
        return "//label[contains(text(),'Benefit & Motivation of Change ')]//..//..//div[@id='X8View']";
    }

    public static String PriorityofChangeDropDwn() {
        return "//label[contains(text(),'Priority of Change:')]//..//..//input[@id='X11']";
    }

    public static String PlatformDropDwn() {
        return "//label[contains(text(),'Platform')]//..//..//input[@id='X21']";
    }

    public static String CategoryDropDwn() {
        return "//label[contains(text(),'Category')]//..//..//input[@id='X29']";
    } // 

    public static String EnvironmentDropDwn() {
        return "//label[contains(text(),'Environment :')]//..//..//input[@id='X14']";
    }

    public static String ImplementationSiteDroopDwn() {
        return "//label[contains(text(),'Implementation Site:')]//..//..//input[@id='X27']";
    }

    public static String TypeofRequestDropDwn() {
        return "//label[contains(text(),'Type of Request:')]//..//..//input[@id='X32']";
    }

    public static String OpenChangeDesNextBtn() {
        return "//button[contains(text(),'Next ')]";
    }

    ///////Select the Required Implementation Dates
    public static String SelectRequiredDatesWait() {
        return "//input//..//span//span[contains(text(),'Select the Required Implementation Dates')]";
    }

    public static String PlannedStartbtn() {
        return "//div[@class='xButton']//a[@id='X6Icon']//img[@title='Open Calendar']";
    }

    public static String StartDateBtn() {
        return "//span[text()='22']";
    }

    public static String StartDateOK() {
        return "//button[@id='okButton']";
    }

    public static String PlanedEndDateBtn() {
        return "//label[contains(text(),'Planned End:')]//..//..//div[@id='X21Edit']//img[@title='Open Calendar']";
    }

    public static String EndDateBtn() {
        return "//span[text()='24']";
    }

    public static String ScheduleDowntimeBtn() {
        return "//label[contains(text(),'No Schedule Downtime')]/../..//label[@class='xCheckboxIcon']";
    }

    /////Open_Change_Details
    public static String OpenChangeDetailsWait() {
        return "//span[@id]//span[contains(text(),'Open Change')]";
    }

    public static String IMPLEMENTING_EXEC() {
        return "//label[contains(text(),'IMPLEMENTING EXEC')]//..//../input//..//div//div//div//div//input[@id='X6']";
    }

    public static String REQUESTING_EXEC() {
        return "//label[contains(text(),'REQUESTING EXEC')]//..//..//..//div//div//div//div//input[@id='X11']";
    }

    public static String CHANGE_OWNER_EXEC() {
        return "//label[contains(text(),'CHANGE OWNER EXEC')]/.//.././/..//input[@id='X18']";
    }

    public static String SYSTEM_OWNER_EXEC() {
        return "//label[contains(text(),'SERVICE/ SYSTEM OWNER EXEC')]/.//.././/..//input[@id='X25']";
    }

    public static String Change_Purpose() {
        return "//label[contains(text(),'Change Purpose:')]/.//.././/..//input[@id='X27']";
    }

    public static String Risk_Level() {
        return "//label[contains(text(),'Risk Level:')]/.//.././/..//input[@id='X13']";
    }

    public static String Impact() {
        return "//label[contains(text(),'Impact:')]/.//.././/..//input[@id='X20']";
    }

    //////Required Business Cluster
    public static String Required_Business_ClusterWait() {
        return "//span[@id]//span[contains(text(),'Select Required Business Cluster(s)')]";
    }

    public static String ADMCheckBox() {
        return "//label[contains(text(),'ADM')]/../..//div[1]//label";
    }

    /////Add Implementor/Approver Details
    public static String Implementor_Approver_DetailsPage() {
        return "//span[@id]//span[contains(text(),'Add Implementor/Approver Details')]";
    }

    public static String Change_Implementer() {
        return "//label[contains(text(),'Change Implementer:')]//..//..//input[@id='X7']";
    }

    public static String Search_Implementer() {
        return "//img[@id='X7FillButton']";
    }

    public static String Service_ManagerFrame1() {
        return "//iframe[contains(@title, 'Select')]";
    }

    public static String Add_Required_ApproverBtn() {
        return "//div[contains(text(),'Add Required Approver(s)')]";
    }

    /////Please Enter an Employee CC/NB and/or Group Name:
    public static String Employee_CC_NB_GroupPage() {
        return "//span[@id]//span[contains(text(),'Please Enter an Employee CC/NB and/or Group Name:')]";
    }

    public static String Employee_CC_NB() {
        return "//label[contains(text(),'Employee No  CC/NB:')]/../..//div[1]/div/div[1]/input[@id='X8']";
    }

    public static String CC_NB_Search() {
        return "//label[contains(text(),'Employee No  CC/NB:')]/../..//div[1]/div[@id]/div//img";
    }

    public static String Group_Name() {
        return "//label[contains(text(),'Group Name:')]//../..//div//input[@id='X12']";
    }

    public static String Group_Search() {
        return "//label[contains(text(),'Group Name:')]//../..//div[2]/div[@id]/div//img";
    }

    public static String Approval_Pop_Up() {
        return "//h2[contains(text(),'Information')]";
    }

    public static String Approval_Text(String EmployeeNoCCNB) {
        return "//span[contains(text(),'Approval : " + EmployeeNoCCNB + " Found. Click Next to Add to the List')]";
    }

    public static String Approval_Okbtn() {
        return "//button[@id='o']";
    }

    public static String Approval_Message() {
        return "//p[@id='commonMsg'][contains(text(), 'Approval :')]/../../..//div[@id][contains(@class, 'messageTrayElement')]";
    }

    public static String Approval_Message_Text() {
        return "//p[@id='commonMsg'][contains(text(), 'Approval :')]";
    }

    public static String last_name_Search() {
        return "//input[contains(@name, 'last.name')]";
    }

    public static String Search_Btn() {
        return "//td//tr//button[@id][1][contains(text(),'Search')]";

        //button[contains(text(),'Search')]";
    }

    public static String SearchPopUpFarme() {
        return "//iframe[@id='popupFrame']";
    }

    public static String Search_Contact_Select() {
        return "//a[@class='firstColumnColor'][contains(text(),'NB207516')]";
    }

    public static String EmployeeID_Wait() {
        return "//span[@title='Employee ID']";
    }

    public static String Add_Required_Approvers_table() {
        return "//div[@class='rowFillBox']";
    }

    public static String Validate_searched_Implementor() {
        return "//input[@value='JUAN,CHANTELLE']";
    }

    public static String Required_ImplementerDeafault_Frame() {
        return "//iframe[contains(@title, 'Wizard')]";
    }

    public static String ContactInfoIframe() {
        return "//iframe[@id='popupFrame']";
    }

    public static String Add_Reviewer_Details_Page() {
        return "//label[@id][contains(text(),'Add Required Reviewers')]";
    }

    //............Add Affected Assets/CIs page 
    public static String Add_Affected_AssetsPage() {
        return "//span[@id]//span[contains(text(),'Add Affected Assets/CIs')]";
    }

    public static String System_Application_Name() {
        return "//span[@id]//label[contains(text(),'System/Application Name:')]//../..//input[@id]/..//div[1]/input[@id='X7']";
    }

    public static String No_of_Components() {
        return "//input[@name='instance/components.no']";
    }

    public static String Application_Name_DropDwn() {
        return "//div[@id='X7Popup_1'][contains(text(),'3D SECURE ISSUING')]";
    }

    //...............Open Change Page    
    public static String Open_ChangePage() {
        return "//span[@id]//span[contains(text(),'Open Change')]";
    }

    public static String authorize_form() {
        return "//input[@name='instance/file.authorize.form']";
    }

    public static String Scheduling_changes() {
        return "//input[@name='instance/scheduled.changes']";
    }

    public static String D2D_changes() {
        return "//input[@name='instance/db2.changes']";
    }

    public static String Cics_Change() {
        return "//input[@name='instance/cics.changes']";
    }

    public static String Submition_page() {
        return "//span[contains(text(),'Please Remember to Submit Your')]";
    }

    public static String Finsh_Button() {
        return "//button[contains(text(),'Finish')]";
    }

    public static String Submit_Message() {
        return "//span[@id='X8']/span";
    }

    //..............................Validate Change Request..........
    public static String Description_of_Change() {
        return "//input[@name='instance/header/brief.description.old']//..//input[@value]";
//input[@name='instance/header/brief.description.old']";
    }

    public static String Motivation_of_Change() {
        return "//div[@id='X1719View']//p";
    }

    public static String Priority_of_Change() {
        return "//input[@name='instance/header/priority']";
    }

    public static String Type_of_Request() {
        return "//input[@name='instance/middle/component.name']";
    }

    public static String Planned_Start() {
        return "//input[@name='instance/header/planned.start']";
    }

    public static String Planned_End() {
        return "//input[@name='instance/header/planned.end']";
    }

    public static String Change_Implementor_CCNB() {
        return "//input[@name='instance/alternate.requested.by']";
    }

    public static String Employee_Full_Name() {
        return "//input[@alias='instance/alternate.requested.by.lastname']";
    }

    public static String Risk_Level_Value() {
        return "//input[@name='instance/header/risk.assessment']";
    }

    public static String Impact_Value() {
        return "//input[@name='instance/impact']";
    }

    public static String Change_Purpose_value() {
        return "//input[@name='instance/ned.change.purpose']";
    }

    public static String Platform() {
        return "//input[@name='instance/middle/platform.name']";
    }

    public static String Environment() {
        return "//input[@name='instance/environment']";
    }

    public static String Implementation_Site() {
        return "//input[@name='instance/impl.site']";
    }

    public static String Category() {
        return "//input[@name='instance/it.product']";
    }

    public static String IMPLEMENTING_Name() {
        return "//input[@name='instance/header/work.manager']";
    }

    public static String REQUESTING_Name() {
        return "//input[@name='instance/middle/contact.name.2']";
    }

    public static String CHANGE_OWNER_Name() {
        return "//input[@name='instance/middle/alternate.contact.name.2']";
    }

    public static String ERVICE_SYSTEM_Name() {
        return "//input[@name='instance/header/businessOwner']";
    }

    public static String Clusters() {
        return "//span[@name='instance/ned.business.units/ned.business.units[1]']";
    }

    //Xpath Ngomso Scheduled Production Downtime Period:
    // ....CM CheckList Xpaths 
    public static String CM_CheckListBTN() {
        return "//a[@title='Notebook tab - CM CheckList']";
    }

    public static String Update_CM_ChecklistBtn() {
        return "//div[contains(text(),'Click to View and Update CM Checklist')]";
    }

    public static String CMCheckListOkBtn() {
        return "//button[@id='ext-gen-top1009']";
    }

    public static String CM_CheckList_Questions() {
        return "//input[@name='instance/q19.ans']";
    }

    public static String CM_CheckList_QuestionsAndNum(int QuestionNum) {
        return "//input[@name='instance/q" + QuestionNum + ".ans']";
    }

    public static String nedCMChecklistFrame() {
        return "//iframe[@src='detail.do?preserveMessage=1']";
//iframe[@id='mif-comp-ext-gen-top1006-547990']";
    }

    public static String nedCMChecklistAprovalPopUp() {
        return "//span[contains(text(),'You have unsaved changes to this record. Save them now?')]";
    }

    public static String CMChecklistAprovalYesBtn() {
        return "//button[@id='y']";
    }

    public static String nedCMChecklistComments(int CommentNum) {
        return "//input[@name='instance/q" + CommentNum + ".comments']";
    }

    public static String nedCMChecklistComments() {
        return "//input[@name='instance/q7.comments']";
    }

    public static String nedCMChecklistUpadatedText() {
        return "//p[@id='commonMsg'][contains(text(), 'nedCMChecklist record updated.')]";
    }
    //.......Finalise PAge 

    ///
    public static String Change_confirm_Message() {
        return "//p[@id='commonMsg'][contains(text(),'Change')]";
    }

    public static String PopUp_Change_Number() {
        return "//span[@class='ext-mb-text']";
    }

    public static String FinaliseSaveBtn() {
        return "//button[@id='ext-gen-top1024']";
    }

    public static String FinaliseAlert() {
        return "//h2[contains(text(),'Information')]";
    }

    public static String Submit_ChangeBtn() {
        return "//button[@id][contains(text(),'Submit Change')]";
    }
    //    public static String ""()
//    {
//        return "";
//    }
    //    public static String ""()
//    {
//        return "";
//    }
    //    public static String ""()
//    {
//        return "";
//    }
    //    public static String ""()
//    {
//        return "";
//    }
    //    public static String ""()
//    {
//        return "";
//    }

}
