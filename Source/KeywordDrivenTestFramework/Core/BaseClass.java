/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Core;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Reporting.ReportGenerator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import KeywordDrivenTestFramework.Utilities.AutoItDriverUtility;
import KeywordDrivenTestFramework.Utilities.DataBaseUtility;
import KeywordDrivenTestFramework.Utilities.OtpSmsUtility;
import KeywordDrivenTestFramework.Utilities.PerformanceMonitor;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.core.har.Har;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.openqa.selenium.Proxy;

/**
 *
 * @author fnell
 */
// All tests inherit from base class, 
// Base class contains initialisations of all neccessary utilities and 
// entities
@KeywordAnnotation(Keyword ="",createNewBrowserInstance = false)
public class BaseClass {

    public static List<TestEntity> testDataList;
    public static Enums.BrowserType browserType;
    public static ReportGenerator reportGenerator;
    
    public static SeleniumDriverUtility SeleniumDriverInstance;
    public static SikuliDriverUtility SikuliDriverInstance;
    public static DataBaseUtility DataBaseInstance;
    public static AutoItDriverUtility AutoItInstance;
    public static OtpSmsUtility otpSms;
    public static ApplicationConfig appConfig = new ApplicationConfig();
    private DateTime startTime, endTime;
    private Duration testDuration;
    public static String testCaseId;
    public static String reportDirectory;
    public static String currentTestDirectory;
    public static Enums.Environment currentEnvironment;
    
    
   
    
    //Test Result Section
    
    
    public static int totalTests, totalPasses, totalFails;
    
    public static List<TestResult> testResults;
    
    
    public static long TotalSeconds = 0;
    public static long TotalMinutes = 0;
    public static long TotalHours = 0;
    
    
    public static TestResult currentResult;
    
    
    //Appium-specific config
    
    public static Enums.Database currentDatabase;
    public static Enums.Device currentDevice;
    public static Enums.DeviceConfig currentDeviceConfig;
    public static Enums.MobilePlatform currentPlatform;
    public static AppiumDriverUtility AppiumDriverInstance;
    public static boolean requiresBrowser = true; //For appium set this false in the @Test
    public static boolean _isRemote = false; //For Remote Testing set to True in @Test
    public static String appiumPort;

    public static String inputFilePath;
    public static String screenshotPath;
    public static String relativeScreenShotPath;
    public TestEntity testData;
    
    public static Narrator narrator;
    
    //Performance monitoring section
    public static PerformanceMonitor performanceMonitor;
    public Proxy seleniumProxy;
    public static boolean EnablePerformanceMonitoring = true; 
    public static BrowserMobProxyServer BMProxy;
    public static Har performanceHar;

    public static String getRelativeScreenshotPath() {
        return  "./" + relativeScreenShotPath;
    }

    public static void setScreenshotPath(String screenshotPath) {
        BaseClass.screenshotPath = screenshotPath;
    }

    public BaseClass() 
    {
        if(testResults == null)
        {
            testResults = new ArrayList<>();
        }
             
        System.setProperty("java.awt.headless", "false");
    }

    public void setStartTime() {
        this.startTime = new DateTime();
    }

    public long getTotalExecutionTime() {
        this.endTime = new DateTime();
        testDuration = new Duration(this.startTime, this.endTime);
        return testDuration.getStandardSeconds();
    }
    
    
    //Test Results functions
    
    public void CalculateTotalTestTime() {
        for (TestResult result : testResults) {
            TotalSeconds += result.testDuration;
        }

        if (TotalSeconds > 60) {
            while (TotalSeconds > 60) {
                TotalMinutes += 1;
                TotalSeconds -= 60;

            }
        }

        if (TotalMinutes > 60) {
            while (TotalMinutes > 60) {
                TotalHours += 1;
                TotalMinutes -= 60;
            }
        }
    }
    public void addResult(TestResult testResult) {
        totalTests += 1;
        if (testResult.testStatus == Enums.ResultStatus.PASS || testResult.testStatus == Enums.ResultStatus.WARNING) {
            totalPasses += 1;
        } else if (testResult.testStatus == Enums.ResultStatus.FAIL) {
            totalFails += 1;
        }
        currentResult = testResult;
        this.testResults.add(testResult);
    }

    public String resolveScenarioName() {
        String isolatedFileNameString;
        String[] splitFileName;
        String[] inputFileNameArray;
        String resolvedScenarioName = "";

        // Get file name from inputFilePath (remove file extension)
        inputFileNameArray = inputFilePath.split("\\.");
        isolatedFileNameString = inputFileNameArray[0];
        if (isolatedFileNameString.contains("/")) {
            inputFileNameArray = isolatedFileNameString.split("/");
        } else if (isolatedFileNameString.contains("\\")) {
            inputFileNameArray = isolatedFileNameString.split("\\\\");
        }

        isolatedFileNameString = inputFileNameArray[inputFileNameArray.length - 1];

        splitFileName = isolatedFileNameString.split("(?=\\p{Upper})");

        for (String word : splitFileName) {
            resolvedScenarioName += word + " ";
        }

        return resolvedScenarioName.trim();
    }

    public String retrieveTestParameterUsingTestCaseId(String testCaseId, String parameterName) {
        String defaultReturn = "parameter not found";
        for (TestEntity testEntity : this.testDataList) {
            if (testEntity.TestCaseId.toUpperCase().equals(testCaseId.toUpperCase())) {
                if (testEntity.TestParameters.containsKey(parameterName)) {
                    return testEntity.TestParameters.get(parameterName);
                } else {
                    return defaultReturn;
                }
            }
        }
        return defaultReturn;
    }
    
    public void pause(int milisecondsToWait) {
        try {
            Thread.sleep(milisecondsToWait);
        } catch (Exception e) {

        }
    }
    
    public String generateDateTimeString() {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");

        return dateFormat.format(dateNow).toString();
    }

}
